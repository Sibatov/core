# CORE APP ON REACT

This app develops in TypeScript, Redux, ESLint, Prettier, Styled-Components

## Available Scripts

In the project directory, you can run:

### `npm start:dev`

Runs the app and builds in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.\

### `npm start:production`

Runs the app and builds in the development mode.

### `npm eslint`
Runs linter with prettier and clean code.
