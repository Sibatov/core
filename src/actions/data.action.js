import {SET_DATA, GET_DATA, ERROR_DATA} from '../constants';
import axios from 'axios';

export const getDataAction = (data) => ({
  type: SET_DATA,
  payload: data,
});

export const getData = () => async (dispatch) => {
  try {
    const res = await axios.get('https://jsonplaceholder.typicode.com/users');
    dispatch({
      type: GET_DATA,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: ERROR_DATA,
      payload: error,
    });
  }
};
