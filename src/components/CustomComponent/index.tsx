import React from 'react';

import {useDispatch} from 'react-redux';
import {getData} from '../../actions/data.action';

import {Custom} from './style';

function CustomComponent() {
  const dispatch = useDispatch();
  // const data = useSelector(state => state.data)

  const getDataHandle = () => {
    dispatch(getData());
  };

  return (
    <Custom>
      this is custom div
      <button type="button" onClick={getDataHandle}>get Data</button>
    </Custom>
  );
}

export default CustomComponent;
