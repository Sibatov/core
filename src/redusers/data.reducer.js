const initialState = {
  data: [],
  error: '',
};

export function dataReducer(state = initialState, action) {
  switch (action.type) {
  case 'GET_DATA':
    return {...state, data: action.payload};
  case 'SET_DATA':
    return {...state, data: action.payload};
  case 'ERROR_DATA':
    return {...state, error: action.payload};
  default:
    return state;
  }
}
